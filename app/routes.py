from app import app

import board
import neopixel

pixels = neopixel.NeoPixel(board.D21, 24)

@app.route('/')
@app.route('/index')
def index():
    '''Response for the main page'''
    return "Hello, World!"

@app.route('/on')
def light_on():
    '''Turn on the ring light'''
    for pixel in range(0,23):
        pixels[pixel] = (255,128,64)
    return "Ring light should be on"

@app.route('/off')
def light_off():
    '''Turn off the ring light'''
    for pixel in range(0,23):
        pixels[pixel] = (255,128,64)
    return "Ring light should be off"
